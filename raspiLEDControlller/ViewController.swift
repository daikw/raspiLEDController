//
//  ViewController.swift
//  raspiLEDControlller
//
//  Created by Daiki Watanabe on 2017/11/07.
//  Copyright © 2017年 Daiki Watanabe. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    // MARK: - Properties
    var centralManager: CBCentralManager!
    var peripheral: CBPeripheral!
    var targetCharacteristic: CBCharacteristic!
    var led_state: Bool = false // true means on, false does off
    
    
    // MARK: - Default methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //  required: Called whenever the connection state changes.
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print ("state: \(central.state)")
    }
    
    
    // MARK: - Actions
    
    // Start Scan
    @IBAction func Connect(_ sender: UIButton) {
        centralManager.scanForPeripherals(withServices: nil, options: nil)
    }
    
    // Send and write the text
    @IBAction func switchLED(_ sender: UIButton) {
        led_state = !led_state
        
        var led_sendData: Data
        if led_state {
            led_sendData = Data(bytes: [1])
        } else {
            led_sendData = Data(bytes: [0])
        }
        
        if let target = targetCharacteristic {
            peripheral.writeValue(led_sendData, for: target, type: CBCharacteristicWriteType.withResponse)
        }
    }

    
    // MARK: - Event handlers of the central manager
    
    // Called when any peripheral is discovered.
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Discovered BLE device: \(peripheral)")
        print("RSSI: \(RSSI)")
        
        if let name = peripheral.name {
            // select raspi from the discovered peripherals
            if name == "raspberrypi" {
                // When the target was discovered, scan should be stopped.
                self.centralManager.stopScan()
                
                // Start connection
                self.peripheral = peripheral
                self.centralManager.connect(self.peripheral, options: nil)
            }
        }
    }
    
    // Called when connection succeeded.
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connect success!")
        
        print("Discovering services...")
        peripheral.delegate = self
        peripheral.discoverServices(nil)
    }
    
    // Called when connection failed.
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Connect failed...")
    }
    
    
    // MARK: - Event handlers of connected peripheral
    
    // Called when services were discovered.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else{
            print("Any services was not discovered.")
            return
        }
        print("\(services.count) services were discovered in the peripheral: \(peripheral)")
        
        // Discover characteristics from the discovered services.
        for service in services {
            if service.uuid.isEqual(CBUUID(string: "ff00")) {
                print("The target service was discovered.")
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    // Called when characteristics were discovered.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else {
            print("Any characteristic was not discovered.")
            fatalError()
        }
        print("\(characteristics.count) characteristics were discovered in the service: \(service)")
        
        for characteristic in characteristics {
            if characteristic.uuid.isEqual(CBUUID(string: "ff11")) {
                print("The target characteristic was discovered.")
                
                // Store the targets.
                self.targetCharacteristic = characteristic
                self.peripheral = peripheral
            }
        }
    }
    
    // Called when reading the characteristic were finished.
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Reading characteristic was successfull!)")
        print("Service UUID: \(characteristic.service.uuid)")
        print("Characteristic UUID: \(characteristic.uuid)")
        print("Characteristic UUID: \(characteristic.uuid)")
        print("value: \(String(describing: characteristic.value))")
    }
    
    // Called when writing the characteristic were finished.
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Writing characteristic was successfull!")
    }
}

